!!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!! NEMO/TOP1 :  1 - tracer definition                     (namtrc    )
!!              2 - tracer data initialisation            (namtrc_dta)
!!              3 - tracer advection                      (namtrc_adv)
!!              4 - tracer lateral diffusion              (namtrc_ldf)
!!              5 - tracer vertical physics               (namtrc_zdf)
!!              6 - tracer newtonian damping              (namtrc_dmp)
!!              7 - dynamical tracer trends               (namtrc_trd)
!!              8 - tracer output diagonstics             (namtrc_dia)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&namtrc     !   tracers definition
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   nn_dttrc      =  1        !  time step frequency for passive sn_tracers      
   nn_writetrc   =  5475     !  time step frequency for sn_tracer outputs
   ln_rsttr      = .false.   !  start from a restart file (T) or not (F)
   nn_rsttr      =   0       !  restart control = 0 initial time step is not compared to the restart file value
                             !                  = 1 do not use the value in the restart file
                             !                  = 2 calendar parameters read in the restart file
   cn_trcrst_in  = "restart_trc_in"   !  suffix of pass. sn_tracer restart name (input)
   cn_trcrst_out = "restart_trc"      !  suffix of pass. sn_tracer restart name (output)
   ln_trcdta     =  .true.   !  Initialisation from data input file (T) or not (F)
   ln_trcdmp     =  .false.  !  add a damping termn (T) or not (F)
!
!                !    name   !           title of the field              ! initial data ! initial data ! save   !
!                !           !                                           !  units       ! from file    ! or not ! 
!                !           !                                           !              ! or not       !        !
   sn_tracer(1)   = 'DIC     ' , 'Dissolved inorganic Concentration      ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(2)   = 'TAlk    ' , 'Total Alkalinity Concentration         ',  'eq/L '   ,  .true.     ,  .true.
   sn_tracer(3)   = 'O2      ' , 'Dissolved Oxygen Concentration         ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(4)   = 'CaCO3   ' , 'Calcite Concentration                  ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(5)   = 'POC     ' , 'Small organic carbon Concentration     ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(6)   = 'PHYC    ' , 'Nanophytoplankton C Concentration      ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(7)   = 'PHYN    ' , 'Nanophytoplankton N Concentration      ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(8)   = 'PHYFe   ' , 'Nanophytoplankton Fe Concentration     ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(9)   = 'NCHL    ' , 'Nano chlorophyl Concentration          ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(10)  = 'PHY2C   ' , 'Diatoms C Concentration                ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(11)  = 'PHY2N   ' , 'Diatoms N Concentration                ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(12)  = 'PHY2Fe  ' , 'Diatoms Fe Concentration               ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(13)  = 'DCHL    ' , 'Diatoms chlorophyl Concentration       ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(14)  = 'ZOO     ' , 'Microzooplankton Concentration         ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(15)  = 'ZOO2    ' , 'Mesozooplankton Concentration          ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(16)  = 'dFe     ' , 'Dissolved Iron Concentration           ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(17)  = 'GOC     ' , 'Big organic carbon Concentration       ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(18)  = 'NO3     ' , 'Nitrates Concentration                 ',  'mol-C/L' ,  .true.     ,  .true.
   sn_tracer(19)  = 'NH4     ' , 'Ammonium Concentration                 ',  'mol-C/L' ,  .false.    ,  .true.
   sn_tracer(20)  = 'agessc  ' , 'Ideal Age                              ',  'years'   ,  .false.    ,  .true.
/
!-----------------------------------------------------------------------
&namtrc_dta      !    Initialisation from data input file
!-----------------------------------------------------------------------
!
!                !  file name               ! frequency (hours) ! variable   ! time interp. !  clim  ! 'yearly'/ ! weights  ! rotation !
!                !                          !  (if <0  months)  !   name     !   (logical)  !  (T/F) ! 'monthly' ! filename ! pairing  !
   sn_trcdta(1)  = 'data_dic_nomask'        ,        -12        ,  'DIC'     ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(2)  = 'data_alkalini_nomask'   ,        -12        ,  'Alkalini',    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(3)  = 'data_o2_nomask'         ,        -1         ,  'O2'      ,    .true.    , .true. , 'yearly'  , ''       , ''
   sn_trcdta(16) = 'data_fer_nomask'        ,        -12        ,  'Fer'     ,    .false.   , .true. , 'yearly'  , ''       , ''
   sn_trcdta(18) = 'data_no3_nomask'        ,        -1         ,  'NO3'     ,    .true.    , .true. , 'yearly'  , ''       , ''
!
   cn_dir        =  './'      !  root directory for the location of the data files
   rn_trfac(1)   =   1.0e-06   ! In this version DIC and TA will remain in mol/L
   rn_trfac(2)   =   1.0e-06   ! while other tracers will be in mmol m^-3 except
   rn_trfac(3)   =  44.6      ! Fe ones which are in nmol m^-3 (pmol/L)
   rn_trfac(16)  =   1.0e+12  ! convert dFe from mol/L to pmol/L
   rn_trfac(18)  =   1.0      ! NO3 is in N units
/
!-----------------------------------------------------------------------
&namtrc_adv    !   advection scheme for passive tracer 
!-----------------------------------------------------------------------
   ln_trcadv_cen2   =  .false.  !  2nd order centered scheme   
   ln_trcadv_tvd    =  .false.  !  TVD scheme
   ln_trcadv_muscl  =  .true.   !  MUSCL scheme
   ln_trcadv_muscl2 =  .false.  !  MUSCL2 scheme + cen2 at boundaries
   ln_trcadv_ubs    =  .false.  !  UBS scheme
   ln_trcadv_qck    =  .false.  !  QUICKEST scheme
/
!-----------------------------------------------------------------------
&namtrc_ldf    !   lateral diffusion scheme for passive tracer 
!-----------------------------------------------------------------------
   ln_trcldf_diff   =  .true.   !  performs lateral diffusion (T) or not (F)
!                               !  Type of the operator : 
   ln_trcldf_lap    =  .true.   !     laplacian operator       
   ln_trcldf_bilap  =  .false.  !     bilaplacian operator     
                                !  Direction of action  :
   ln_trcldf_level  =  .false.  !     iso-level                
   ln_trcldf_hor    =  .false.  !     horizontal (geopotential)         (require "key_ldfslp" when ln_sco=T)
   ln_trcldf_iso    =  .true.   !     iso-neutral                       (require "key_ldfslp")
!                               !  Coefficient
   rn_ahtrc_0       =  1000.    !  horizontal eddy diffusivity for tracers [m2/s]
   rn_ahtrb_0       =     0.    !     background eddy diffusivity for ldf_iso [m2/s]
/
!-----------------------------------------------------------------------
&namtrc_zdf        !   vertical physics
!-----------------------------------------------------------------------
   ln_trczdf_exp   =  .false.  !  split explicit (T) or implicit (F) time stepping
   nn_trczdf_exp   =   3       !  number of sub-timestep for ln_trczdfexp=T
/
!-----------------------------------------------------------------------
&namtrc_rad        !  treatment of negative concentrations 
!-----------------------------------------------------------------------
   ln_trcrad   =  .true.  !  artificially correct negative concentrations (T) or not (F)
/
!-----------------------------------------------------------------------
&namtrc_dmp    !   passive tracer newtonian damping   
!-----------------------------------------------------------------------
   nn_hdmp_tr  =   -1      !  horizontal shape =-1, damping in Med and Red Seas only
                           !                   =XX, damping poleward of XX degrees (XX>0)
                           !                      + F(distance-to-coast) + Red and Med Seas
   nn_zdmp_tr  =    1      !  vertical   shape =0    damping throughout the water column
                           !                   =1 no damping in the mixing layer (kz  criteria)
                           !                   =2 no damping in the mixed  layer (rho crieria)
   rn_surf_tr  =   50.     !  surface time scale of damping   [days]
   rn_bot_tr   =  360.     !  bottom  time scale of damping   [days]
   rn_dep_tr   =  800.     !  depth of transition between rn_surf and rn_bot [meters]
   nn_file_tr  =    0      !  create a damping.coeff NetCDF file (=1) or not (=0)
/
!-----------------------------------------------------------------------
&namtrc_trd       !   diagnostics on tracer trends        ('key_trdtrc')
!                          or mixed-layer trends          ('key_trdmld_trc')
!----------------------------------------------------------------------
   nn_trd_trc  =  5475      !  time step frequency and tracers trends
   nn_ctls_trc =   0        !  control surface type in mixed-layer trends (0,1 or n<jpk)
   rn_ucf_trc  =   1        !  unit conversion factor (=1 -> /seconds ; =86400. -> /day)
   ln_trdmld_trc_restart = .false.  !  restart for ML diagnostics
   ln_trdmld_trc_instant = .true.  !  flag to diagnose trends of instantantaneous or mean ML T/S
   ln_trdtrc(1)  =   .true.
   ln_trdtrc(2)  =   .true.
   ln_trdtrc(23) =   .true.
/
!-----------------------------------------------------------------------
&namtrc_dia       !   parameters for passive tracer additional diagnostics
!----------------------------------------------------------------------
   ln_diatrc     =  .true.   !  save additional diag. (T) or not (F)
   nn_writedia   =  5475     !  time step frequency for diagnostics
/
