!!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!! PISCES  :    1  - air-sea exchange                         (nampisext)
!! namelists    2  - biological parameters                    (nampisbio)
!!              3  - parameters for nutrient limitations      (nampislim)    
!!              4  - parameters for phytoplankton             (nampisprod,nampismort)
!!              5  - parameters for zooplankton               (nampismes,nampiszoo)
!!              6  - parameters for remineralization          (nampisrem)
!!              7  - parameters for calcite chemistry         (nampiscal)
!!              8  - parameters for inputs deposition         (nampissed)
!!              9  - parameters for Kriest parameterization   (nampiskrp, nampiskrs)
!!              10 - additional 2D/3D  diagnostics            (nampisdia)
!!              11 - Damping                                  (nampisdmp)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisext     !   air-sea exchange
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   ln_co2int  =  .false. ! read atm pco2 from a file (T) or constant (F)
   atcco2     =  284.32  ! Constant value atmospheric pCO2 - ln_co2int = F
   clname     =  'atcco2.txt'  ! Name of atm pCO2 file - ln_co2int = T
   nn_offset  =  0       ! Offset model-data start year - ln_co2int = T
!                        ! If your model year is iyy, nn_offset=(years(1)-iyy) 
!                        ! then the first atmospheric CO2 record read is at years(1)
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisatm     !  Atmospheric prrssure 
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
!              !  file name   ! frequency (hours) ! variable   ! time interp. !  clim  ! 'yearly'/ ! weights  ! rotation !
!              !              !  (if <0  months)  !   name     !   (logical)  !  (T/F) ! 'monthly' ! filename ! pairing  !
   sn_patm     = 'presatm'    ,     -1            , 'patm'     ,  .true.      , .true. ,   'yearly'  , ''       , ''
   cn_dir      = './'     !  root directory for the location of the dynamical files
!
   ln_presatm  = .true.   ! constant atmopsheric pressure (F) or from a file (T)
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisbio     !   biological parameters
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   nrdttrc    =  4        ! time step frequency for biology
   wsbio      =  2.       ! POC sinking speed
   wsbio2     =  30.      ! Big particles sinking speed
   wsbioc     =  20.      ! CaCO3 sinking speed
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampistf     !   parameters for temperature-dependence of various rates
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   AEP        = -4500.    ! Activation energy for phytoplankton
   AEZ        = -4500.    ! Activation energy for microzooplankton
   AEZ2       = -4500.    ! Activation energy for mesozooplankton
   AER        = -6500.    ! Activation energy for remineralization
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisprod     !   parameters for phytoplankton growth
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   QNmax1     = 0.172     ! Small phytoplankton max N quota
   QNmin1     = 0.04      ! Small phytoplankton min N quota
   QNmax2     = 0.172     ! Large phytoplankton max N quota
   QNmin2     = 0.04      ! Large phytoplankton min N quota
   VCNref     = 0.6       ! Reference rate of N uptake
   QFemax1    = 93.08     ! Small phytoplankton max Fe quota
   QFemin1    = 4.65      ! Small phytoplankton min Fe quota
   QFemax2    = 69.81     ! Large phytoplankton max Fe quota
   QFemin2    = 6.5       ! Large phytoplankton min Fe quota
   VCFref     = 79.       ! Reference rate of Fe uptake
   PCref      = 3.        ! Reference rate of photosynthesis
   alphachl   = 1.08      ! Initial slope of P-E curve
   kn1        = 0.1       ! Small P half-saturation for NO3 uptake
   ka1        = 0.05      ! Small P half-saturation for NH4 uptake
   kf1        = 100.      ! Small P half-saturation for Fe uptake
   kn2        = 1.0       ! Large P half-saturation for NO3 uptake
   ka2        = 0.05      ! Large P half-saturation for NH4 uptake
   kf2        = 200.      ! Large P half-saturation for Fe uptake
   thetamax   = 0.18      ! Maximum chlorophyll/nitrogen ratio
   eta        = 2.        ! Metabolic cost of biosynthesis
   kexh       = 1.7       ! Exudation rate of excess intracellular C
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampismort     !   parameters for phytoplankton sinks
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   mprat      =  0.05     ! phytoplankton mortality rate
   mprat2     =  0.1      ! Large zooplankton mortality rate
   mpratm     =  0.1      ! Diatoms mortality rate
   mpqua      =  6.E-2    ! quadratic mortality of phytoplankton
   mpquad     =  6.E-2    ! maximum quadratic mortality of diatoms
   chldegr    =  2.E-2    ! Chlorophyll photooxidation rate
   picfrx     =  0.05     ! CaCO3 production rate (as fraction of POC)
   xminp      =  0.01     ! minimum phytoplankton concentration for linear mortality
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampismes     !   parameters for mesozooplankton
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   part2      = 0.75      ! part of calcite not dissolved in mesozoo guts (not used)
   gmax2      = 0.85      ! maximum grazing rate
   apl        = 0.25      ! large zooplankton functional response parameter
   zsr2       = 0.1       ! specific respiration rate
   lambda2    = 0.8       ! assimilation efficiency
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampiszoo     !   parameters for microzooplankton
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   part       = 0.5       ! part of calcite not dissolved in microzoo guts (not used)
   gmax1      = 1.7       ! maximum grazing rate
   aps        = 0.25      ! small zooplankton functional response parameter
   zsr1       = 0.3       ! specific respiration rate
   lambda1    = 0.8       ! assimilation efficiency
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisrem     !   parameters for remineralization
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   xremik     = 0.25      ! remineralisation rate of POC 
   xremip     = 0.025     ! remineralisation rate of DOC
   nitrif     = 0.05      ! NH4 nitrification rate 
   xlam1      = 0.001     ! scavenging rate of iron (low concentrations)
   xlam2      = 2.5       ! scavenging rate of iron (high concentrations)
   ligand     = 600.      ! ligand concentration
   pocfctr    = 0.66      ! multiplier for POC-dependent scavenging
   o2thresh   = 6.        ! O2 threshold for denitrification
   nh4frx     = 0.25      ! annamox fraction of denitrification
   oxymin     = 1.        ! half saturation constant for anoxia 
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampiscal     !   parameters for Calcite chemistry
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   kdca       = 7.4E-3    ! calcite dissolution rate constant in d^-1 (2700 m e-folding scale for w=20 m/d)
   nca        = 1.        ! order of dissolution reaction (not used)
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampissed     !   parameters for inputs deposition
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
!              !  file name        ! frequency (hours) ! variable   ! time interp. !  clim  ! 'yearly'/ ! weights  ! rotation !
!              !                   !  (if <0  months)  !   name     !   (logical)  !  (T/F) ! 'monthly' ! filename ! pairing  !
   sn_dust     = 'dust.orca'       ,     -1            , 'dust'     ,  .true.      , .true. ,   'yearly'  , ''       , ''
   sn_riverdic = 'river.orca'      ,    -12            , 'riverdic' ,  .false.     , .true. ,   'yearly'  , ''       , ''
   sn_riverdoc = 'river.orca'      ,    -12            , 'riverdoc' ,  .false.     , .true. ,   'yearly'  , ''       , ''
   sn_ndepo    = 'ndeposition.orca',    -12            , 'ndep'     ,  .false.     , .true. ,   'yearly'  , ''       , ''
   sn_ironsed  = 'bathy.orca'      ,    -12            , 'bathy'    ,  .false.     , .true. ,   'yearly'  , ''       , ''
!
   cn_dir      = './'      !  root directory for the location of the dynamical files
   ln_dust     =  .true.   ! boolean for dust input from the atmosphere
   ln_river    =  .false.  ! boolean for river input of nutrients
   ln_ndepo    =  .false.  ! boolean for atmospheric deposition of N
   ln_ironsed  =  .true.   ! boolean for Fe input from sediments
   sedfeinput  =  1000.    ! Coastal release of Iron
   dustsolub   =  0.01     ! Solubility of the dust
   wdust       =  2.0      ! Dust sinking speed
   nitrfix     =  2.25E-2  ! Nitrogen fixation rate
   diazolight  =  50.      ! Diazotrophs sensitivity to light (W/m2)
   concfediaz  =  100.     ! Diazotrophs half-saturation Cste for Iron
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisdia     !   additional 2D/3D tracers diagnostics 
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
!              !    name   !           title of the field          !     units      !
!              !           !                                       !                !  
   pisdia2d(1)  = 'Cflx     ' , 'DIC flux                          ',  'molC/m2/s    '
   pisdia2d(2)  = 'Oflx     ' , 'Oxygen flux                       ',  'molC/m2/s    '
   pisdia2d(3)  = 'Kg       ' , 'Gas transfer                      ',  'mol/m2/s/uatm'
   pisdia2d(4)  = 'Delc     ' , 'Delta CO2                         ',  'uatm         '
   pisdia2d(5)  = 'PMO      ' , 'POC export                        ',  'molC/m2/s    '
   pisdia2d(6)  = 'PMO2     ' , 'GOC export                        ',  'molC/m2/s    '
   pisdia2d(7)  = 'ExpFe1   ' , 'Nano iron export                  ',  'molFe/m2/s   '
   pisdia2d(8)  = 'ExpFe2   ' , 'Diatoms iron export               ',  'molFe/m2/s   '
   pisdia2d(9)  = 'ExpSi    ' , 'Silicate export                   ',  'molSi/m2/s   '
   pisdia2d(10) = 'ExpCaCO3 ' , 'Calcite export                    ',  'molC/m2/s    '
   pisdia2d(11) = 'heup     ' , 'euphotic layer depth              ',  'm            '
   pisdia2d(12) = 'Fedep    ' , 'Iron dep                          ',  'molFe/m2/s   '
   pisdia2d(13) = 'Nfix     ' , 'Nitrogen Fixation                 ',  'molN/m2/s    '
   pisdia3d(1)  = 'PH       ' , 'PH                                ',  '-            '
   pisdia3d(2)  = 'CO3      ' , 'Bicarbonates                      ',  'mol/l        '
   pisdia3d(3)  = 'CO3sat   ' , 'CO3 saturation                    ',  'mol/l        '
   pisdia3d(4)  = 'PAR      ' , 'light penetration                 ',  'W/m2         '
   pisdia3d(5)  = 'PPPHY    ' , 'Primary production of nanophyto   ',  'molC/m3/s    '
   pisdia3d(6)  = 'PPPHY2   ' , 'Primary production of diatoms     ',  'molC/m3/s    '
   pisdia3d(7)  = 'PPNEWN   ' , 'New Primary production of nano    ',  'molC/m3/s    '
   pisdia3d(8)  = 'PPNEWD   ' , 'New Primary production of diat    ',  'molC/m3/s    '
   pisdia3d(9)  = 'PBSi     ' , 'Primary production of Si diatoms  ',  'molSi/m3/s   '
   pisdia3d(10) = 'PFeN     ' , 'Primary production of nano iron   ',  'molFe/m3/s   '
   pisdia3d(11) = 'PFeD     ' , 'Primary production of diatoms iron',  'molFe/m3/s   '
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampisdmp     !  Damping 
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   ln_pisdmp    =  .true.     !  Relaxation of some tracers to a mean value
   nn_pisdmp    =   225       !  Frequency of Relaxation 
/
!'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
&nampismass     !  Mass conservation
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
   ln_check_mass =  .false.    !  Check mass conservation
/
