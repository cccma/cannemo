#!/bin/bash
#===================================================================================================
# Get input files and executables
ln -s ../input/modfiles/* .
cp ${storage_dir}/executables/rebuild_nemo_mpi.exe .
acc_cp mpi_rebuild ${SEQ_EXP_HOME}/bin/mpi_rebuild

# If used, the below section does the parallel rebuild of tiles
if [ "0" -eq "0" ] ; then

  # Get bash arrays with the freq and suffix list
  nemo_hist_file_suffix_list_array=($nemo_hist_file_suffix_list)
  nemo_hist_file_freq_list_array=($nemo_hist_file_freq_list)

  # Check that the lists are the same length
  n_suffix=${#nemo_hist_file_suffix_list_array[@]}
  n_freq=${#nemo_hist_file_freq_list_array[@]}
  if [ "$n_suffix" -ne "$n_freq" ]; then
      bail "ERROR: nemo_hist_file_suffix_list_array and nemo_hist_file_freq_list_array MUST have the same number of elements."
  fi

  # Loop through each element, and setup a rebuild namelist.
  rebuild_list=""
  if [ $nemo_save_hist -eq 1 ] || [ $nemo_rtd -eq 1 ]; then
      for i in $(seq 0 $(($n_suffix-1))); do
          echo "${nemo_hist_file_suffix_list_array[$i]}"
          echo "${nemo_hist_file_freq_list_array[$i]}"
         rebuild_list="$rebuild_list ${cn_exp}_${nemo_hist_file_freq_list_array[$i]}_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_${nemo_hist_file_suffix_list_array[$i]}"
      done
  fi
  histfile_list=$rebuild_list

  # Depending on parameter choices, add relevant restarts to rebuild_list
  end_step=${NEMO_CHUNK_NN_ITEND}
  if [ $pisces_offline -eq 0 ]; then
      rebuild_list="$rebuild_list ${cn_exp}_${end_step}_restart"
      n_rebuild=$(($n_suffix + 1))
      if [ $nn_ice -eq 2 ]; then
          rebuild_list="$rebuild_list ${cn_exp}_${end_step}_restart_ice"
          n_rebuild=$(($n_rebuild + 1))
      fi
  fi
  if [ $nemo_trc -eq 1 ] || [ $nemo_carbon -eq 1 ]; then
      rebuild_list="$rebuild_list ${cn_exp}_${end_step}_restart_trc"
      n_rebuild=$(($n_rebuild + 1))
  fi
  # setup the namelists. The script mpi_rebuild makes the namelists, based on the input of "rebuild_list". This script is adjusted from the
  # default nemo version of rebuild, and produces mulitple namelist files (one for each entry in "rebuild_list" - which corresponds to
  # each mpi task in the parallel rebuild, which gets its own input. (There is one file-type/task [e.g. grid_T] and one namelist/task).
  ${TASK_WORK}/mpi_rebuild "${rebuild_list}" $jpnij
  
  # run the parallel rebuild
  export OMP_NUM_THREADS=5
  #${OMP_NUM_THREADS:-3}
  # The total number for "n" must be <=36 to fit on one node of the XC40
  # This could be tuned, and defitely changed for other machines.
  while [ $(($OMP_NUM_THREADS * $n_rebuild)) -gt 36 ]; do
      OMP_NUM_THREADS=$(($OMP_NUM_THREADS -1))
  done

  # Parallel rebuild execution
  aprun -j 1 -n $n_rebuild -N $n_rebuild -d ${OMP_NUM_THREADS} ${TASK_WORK}/rebuild_nemo_mpi.exe
  wait
fi

if [ $with_nemo_diag -eq 1 ]; then
   # rebuild 1d diaptr files
   rebuild_list2=""
   # number of tiles 
   n_diaptr=`ls -l *diaptr* |wc -l`
   rebuild_list2="$rebuild_list2 ${cn_exp}_1d_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_diaptr"
   # setup the rebuild namelist
   ${TASK_WORK}/mpi_rebuild "${rebuild_list2}" $n_diaptr
   # rebuild diaptr file
   aprun -j 1 -n 1 -N 1 -d ${OMP_NUM_THREADS} ${TASK_WORK}/rebuild_nemo_mpi.exe
   # add scalar and diaptr files to the list being copied to sitestore. 
   histfile_list="$histfile_list ${cn_exp}_1d_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_diaptr"
   if [ ${output_level} -ge 1 ] ; then
     histfile_list="$histfile_list ${cn_exp}_1m_${NEMO_CHUNK_START_DATE}_${NEMO_CHUNK_END_DATE}_scalar_ar6"
   fi
fi

# Since the rebuild is done we can remove links to the tiled files (they will still exist in the run tmp dir)
rm *[0-9][0-9].nc

#=================================
# Create the restart tar archive
#=================================
# This the working directory of this iteration of the run loop.
runwrk=${SEQ_WORKBASE}/CanNEMO/loop_run/nemo_run+${loop_postproc}/work

# tarlist contains a list of additional files to add to the restart archive
tarlist=''

cp -f ${runwrk}/namelist        rs_namelist        || :
cp -f ${runwrk}/namelist_ice    rs_namelist_ice    || :
[ -s rs_namelist ]         && tarlist="$tarlist rs_namelist"
[ -s rs_namelist_ice ]     && tarlist="$tarlist rs_namelist_ice"

if [ $pisces_offline -eq 0 ]; then
  [ -s ${cn_exp}_${NEMO_CHUNK_NN_ITEND}_restart.nc ] && 
     tarlist="$tarlist ${cn_exp}_${NEMO_CHUNK_NN_ITEND}_restart.nc"
  [ -s ${cn_exp}_${NEMO_CHUNK_NN_ITEND}_restart_ice.nc ]  && 
     tarlist="$tarlist ${cn_exp}_${NEMO_CHUNK_NN_ITEND}_restart_ice.nc"
  [ -s nemo_physical_rtd.nc ]  && tarlist="$tarlist nemo_physical_rtd.nc"
  [ -s nemo_ice_rtd.nc ]  && tarlist="$tarlist nemo_ice_rtd.nc"
fi

if [ $nemo_trc -eq 1 ] || [ $nemo_carbon -eq 1 ]; then
  cp -f ${runwrk}/namelist_top    rs_namelist_top    || :
  [ -s rs_namelist_top ]     && tarlist="$tarlist rs_namelist_top"
  tarlist="$tarlist ${cn_exp}_${NEMO_CHUNK_NN_ITEND}_restart_trc.nc"
fi

if [ $nemo_carbon -eq 1 ]; then
  cp -f ${runwrk}/namelist_pisces rs_namelist_pisces || :
  [ -s rs_namelist_pisces ]  && tarlist="$tarlist rs_namelist_pisces"
  [ -s nemo_carbon_rtd.nc ]  && tarlist="$tarlist nemo_carbon_rtd.nc"

  if [ $nemo_cmoc -eq 1 ]; then
    cp -f ${runwrk}/namelist_cmoc rs_namelist_cmoc || :
    [ -s rs_namelist_cmoc ]  && tarlist="$tarlist rs_namelist_cmoc"
  fi
fi

# Save a copy of the executable that was just run to the restart archive
cp -f ${runwrk}/nemo.exe rs_nemo_exec || :
[ -s rs_nemo_exec ] && tarlist="$tarlist rs_nemo_exec"

# Add a file containing the time step
cp -f ${runwrk}/time.step rs_time.step || :
[ -s rs_time.step ] && tarlist="$tarlist rs_time.step"

# Add ocean output and timing info
cp -f ${runwrk}/ocean.output rs_ocean.output || :
[ -s rs_ocean.output ] && tarlist="$tarlist rs_ocean.output"

cp -f ${runwrk}/timing.output rs_timing.output || :
[ -s rs_timing.output ] && tarlist="$tarlist rs_timing.output"

#cp -f rebuild_list rs_saved_history_file_names || :
#[ -s rs_saved_history_file_names ] && tarlist="$tarlist rs_saved_history_file_names"

cp -f ${runwrk}/xnemo_access_log rs_xnemo_access_log || :
[ -s rs_xnemo_access_log ] && tarlist="$tarlist rs_xnemo_access_log"

rm -f NEXT_RESTART_ARC
tar cf NEXT_RESTART_ARC $tarlist ||
  bail "Unable to create restart archive"
rs_out=${cn_exp}_${NEMO_CHUNK_END_DATE}_restart.tar

mv NEXT_RESTART_ARC ../output/$rs_out

for i in $histfile_list; do
  mv ${i}.nc ../output/
done
